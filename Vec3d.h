#ifndef VEC3D
#define VEC3D

#include <math.h>
#include <iostream>

class Vec3d{

   public:
      double x,y,z;
      
      Vec3d(double a=0, double b=0, double c=0);
      ~Vec3d();
      void set(double a, double b, double c); //Set vector components
      void set(const Vec3d &other); //Set equal to other vector
      bool isZero();
      double dot(const Vec3d &other) const; //Get dot product with other vector
      Vec3d cross(const Vec3d &other) const; //Get cross product with other vector
      double angle(const Vec3d &other) const; //Get angle between self and other vector
      double length() const; //Get vector length
      Vec3d unitVector() const; //Get unit vector
      void normalize(); //Normalize
      void scale(const double s); //Scale by a double
      void clampMin(const double c); //Clamp to a minimum value
      void clampMax(const double c); //Clamp to a maximum value
      void clamp(const double a, const double b);
      Vec3d operator/(const double op) const; //Scalar division
      Vec3d operator*(const double op) const; //Scalar multiplication
      Vec3d operator*(const Vec3d &other) const; //Element-wise multiplication
      Vec3d operator+(const double op) const; //Scalar addition
      Vec3d operator+(const Vec3d &other) const; //Vector addition
      Vec3d operator-(const double op) const; //Scalar subtraction
      Vec3d operator-(const Vec3d &other) const; //Vector subtraction
      Vec3d operator-() const; //Vector negation
      void operator*=(const double op); //Scalar multiplication
      void operator+=(const Vec3d &other); //Vector self-addition
      void operator/=(const double op); //Scalar self-division

      friend std::ostream& operator<<(std::ostream& os, const Vec3d& vec){
         os<<"("<<vec.x<<","<<vec.y<<","<<vec.z<<")";
         return os;
      }

      friend std::istream& operator>>(std::istream& is, Vec3d& vec){
         is>>vec.x>>vec.y>>vec.z;
         return is;
      }
};


#endif
