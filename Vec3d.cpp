#include "Vec3d.h"

Vec3d::Vec3d(double a, double b, double c):x(a),y(b),z(c){ }

Vec3d::~Vec3d(){



}

void Vec3d::set(double a, double b, double c){ 
   x=a;
   y=b;
   z=c;
}

void Vec3d::set(const Vec3d &other){ 
   x = other.x; 
   y = other.y;
   z = other.z;
}

bool Vec3d::isZero(){
   return x==0 && y==0 && z==0;
}

double Vec3d::dot(const Vec3d &other) const{ 
   return x*other.x + y*other.y + z*other.z;
}

Vec3d Vec3d::cross(const Vec3d &other) const{ 
   return Vec3d(y*other.z - z*other.y, z*other.x - x*other.z, x*other.y - y*other.x);
}

double Vec3d::angle(const Vec3d &other) const{ 
   return acos(this->dot(other)/(this->length()*other.length()));
}

double Vec3d::length() const{ 
   return sqrt(x*x + y*y + z*z);
}

Vec3d Vec3d::unitVector() const{ 
   double len = length();
   if(len==0){
      std::cerr<<"Error - Divide by zero in calculating unit vector."<<std::endl;
      len = 1;
   }
   return *this/len;
}

void Vec3d::normalize(){ 
   set(unitVector());
}

void Vec3d::scale(const double s){ 
   set(x*s,y*s,z*s);
}

void Vec3d::clampMin(const double c){ 
   x = (x<c)?c:x;
   y = (y<c)?c:y;
   z = (z<c)?c:z;
}

void Vec3d::clampMax(const double c){ 
   x = (x>c)?c:x;
   y = (y>c)?c:y;
   z = (z>c)?c:z;
}

void Vec3d::clamp(const double a, const double b){

   clampMin(a);
   clampMax(b);
}

Vec3d Vec3d::operator/(const double op) const{ 
   if(op==0){
      std::cerr<<"Error - Divide by zero."<<std::endl;
      return *this;
   }
   return Vec3d(x/op,y/op,z/op);
}

Vec3d Vec3d::operator*(const double op) const{ 
   return Vec3d(x*op,y*op,z*op);
}

Vec3d Vec3d::operator*(const Vec3d &other) const{ 
   return Vec3d(x*other.x, y*other.y, z*other.z);
}

Vec3d Vec3d::operator+(const double op) const{ 
   return Vec3d(x+op,y+op,z+op);
}

Vec3d Vec3d::operator+(const Vec3d &other) const{ 
   return Vec3d(x+other.x,y+other.y,z+other.z);
}

Vec3d Vec3d::operator-(const double op) const{ 
   return Vec3d(x-op,y-op,z-op);
}

Vec3d Vec3d::operator-(const Vec3d &other) const{ 
   return Vec3d(x-other.x,y-other.y,z-other.z);
}

Vec3d Vec3d::operator-() const{ 
   return Vec3d(-x,-y,-z);
}

void Vec3d::operator*=(const double op){ 
   set(x*op,y*op,z*op);
}

void Vec3d::operator+=(const Vec3d &other){ 
   set(x+other.x,y+other.y,z+other.z);
}

void Vec3d::operator/=(const double op){ 
   if(op==0){
      std::cerr<<"Error - Divide by zero."<<std::endl;
   }else
      set(x/op, y/op, z/op);
}

