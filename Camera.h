#ifndef CAMERA
#define CAMERA

#include "Vec3d.h"

#include <iostream>
using namespace std;

class Camera{

   public:
      string name;
      
      Camera(string n);
      Camera (int w, int h, double worldX, double worldY, Vec3d pos);
      ~Camera();
      virtual void parse();
      int getpx();
      int getpy();
      double getWorldX();
      double getWorldY();
      Vec3d getviewpt();
      void dumper();

   private:
      int pixel_dim_w, pixel_dim_h;
      double world_dim_w, world_dim_h;
      Vec3d view_point;

};

#endif
