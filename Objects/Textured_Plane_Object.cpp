#include "Textured_Plane_Object.h"
#include "../Model.h"

Textured_Plane_Object::Textured_Plane_Object(string n) : Finite_Plane_Object(n){
   mode = 0;
   texture = NULL;
}

Textured_Plane_Object::~Textured_Plane_Object(){

   if(texture) delete texture;

}

void Textured_Plane_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="normal"){
         cin>>normal;
      }else if(ID=="point"){
         cin>>point;
      }else if(ID=="xdir"){
         cin>>xdir;
      }else if(ID=="dimension"){
         cin>>dimX>>dimY;
      }else if(ID=="textname"){
         string texName;
         cin>>texName;
         texture = new Texture(texName);
      }else if(ID=="mode"){
         cin>>mode;
      }else{
         cerr<<"Incorrect Textured_Plane_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }

   projxdir = xdir - normal.unitVector() * xdir.dot(normal.unitVector());
   projxdir.normalize();
  
   rot[0] = projxdir;
   rot[2] = normal.unitVector();
   rot[1] = rot[2].cross(rot[0]);

}

Vec3d Textured_Plane_Object::getamb() const{
   Vec3d amb = Object::getamb();
   Vec3d texel;
   texture_map(texel);
   
   return amb * texel;
}

Vec3d Textured_Plane_Object::getdiff() const{
   Vec3d diff = Object::getdiff();
   Vec3d texel;
   texture_map(texel);
   
   return diff * texel;
}

void Textured_Plane_Object::texture_map(Vec3d &texel) const{

   switch(mode){
      case 0:
         texture->texture_fit(rotHitPt->x/dimX, rotHitPt->y/dimY, texel);
         break;
      case 1:
         texture->texture_tile(*rotHitPt, texel);
         break;
   }

}

void Textured_Plane_Object::connectObjects(const Model *model){
   Finite_Plane_Object::connectObjects(model);
   texture->setPixelSize(model->cam->getWorldX() / model->cam->getpx(), model->cam->getWorldY() / model->cam->getpy());
}

void Textured_Plane_Object::dumper() const{
   Finite_Plane_Object::dumper();
   cerr<<"---Mode: "<<mode<<endl;
   texture->dumper();
}
