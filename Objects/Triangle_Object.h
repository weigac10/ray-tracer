#ifndef Triangle_OBJECT
#define Triangle_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Triangle_Object : public Object{

      public:
         Triangle_Object();
         ~Triangle_Object();
         void parse();
         virtual double hittest(const Ray ray);
         virtual Vec3d getNormal() const;
         virtual void dumper() const;

      private:
         Vec3d normal, points[3];

};

#endif
