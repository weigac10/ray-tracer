#ifndef Plane_OBJECT
#define Plane_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Plane_Object : public Object{

      public:
         Plane_Object(string n);
         ~Plane_Object();
         void parse();
         virtual double hittest(const Ray ray);
         virtual Vec3d getNormal() const;
         virtual void dumper() const;

      protected:
         Vec3d normal, point;

};

#endif
