#ifndef Sphere_OBJECT
#define Sphere_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Sphere_Object : public Object{

      public:
         Sphere_Object(string n);
         ~Sphere_Object();
         void parse();
         virtual double hittest(const Ray ray);
         virtual Vec3d getNormal() const;
         virtual void dumper() const;

      private:
         Vec3d center;
         double radius;

};

#endif
