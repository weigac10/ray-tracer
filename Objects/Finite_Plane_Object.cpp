#include "Finite_Plane_Object.h"
#include "../Model.h"

Finite_Plane_Object::Finite_Plane_Object(string n) : Plane_Object(n){
   xdir = Vec3d(1,0,0);
   dimX = dimY = 1;
   rotHitPt = new Vec3d();
}

Finite_Plane_Object::~Finite_Plane_Object(){

   if(rotHitPt) delete rotHitPt;

}

void Finite_Plane_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="normal"){
         cin>>normal;
      }else if(ID=="point"){
         cin>>point;
      }else if(ID=="xdir"){
         cin>>xdir;
      }else if(ID=="dimension"){
         cin>>dimX>>dimY;
      }else{
         cerr<<"Incorrect Finite_Plane_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }

   projxdir = xdir - normal.unitVector() * xdir.dot(normal.unitVector());
   projxdir.normalize();
  
   rot[0] = projxdir;
   rot[2] = normal.unitVector();
   rot[1] = rot[2].cross(rot[0]);
}

double Finite_Plane_Object::hittest(const Ray ray){
   double hitDist = Plane_Object::hittest(ray);
   if(hitDist == -1) return -1;
   
   Vec3d newloc = *hitPt - point;
   *rotHitPt = Vec3d(newloc.dot(rot[0]), newloc.dot(rot[1]), newloc.dot(rot[2]));
   if (rotHitPt->x >= 0 && rotHitPt->x <= dimX && rotHitPt->y >= 0 && rotHitPt->y <= dimY)
      return hitDist;

   return -1;
}

void Finite_Plane_Object::dumper() const{
   Plane_Object::dumper();
   cerr<<"---XDir: "<<xdir<<endl;
   cerr<<"---Dimensions: "<<dimX<<"x"<<dimY<<endl;
}
