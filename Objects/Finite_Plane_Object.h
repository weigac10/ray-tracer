#ifndef FINITE_PLANE_OBJECT
#define FINITE_PLANE_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "Plane_Object.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Finite_Plane_Object : public Plane_Object{

      public:
         Finite_Plane_Object(string n);
         ~Finite_Plane_Object();
         void parse();
         virtual double hittest(const Ray ray);
         virtual void dumper() const;

      protected:
         double dimX, dimY;
         Vec3d rot[3];
         Vec3d *rotHitPt;
         Vec3d xdir, projxdir;

};

#endif
