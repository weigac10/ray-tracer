#ifndef TEXTURED_PLANE_OBJECT
#define TEXTURED_PLANE_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Finite_Plane_Object.h"
#include "../Texture.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Textured_Plane_Object : public Finite_Plane_Object{

      public:
         Textured_Plane_Object(string n);
         ~Textured_Plane_Object();
         void parse();
         virtual Vec3d getamb() const;
         virtual Vec3d getdiff() const;
         void texture_map(Vec3d &texel) const;
         virtual void connectObjects(const Model *model);
         virtual void dumper() const;

      private:
         int mode;
         Texture *texture;

};

#endif
