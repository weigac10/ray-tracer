#include "Tiled_Plane_Object.h"
#include "../Model.h"

Tiled_Plane_Object::Tiled_Plane_Object(string n) : Plane_Object(n){
   xdir = Vec3d(1,0,0);
   dimX = dimY = 1;

   projxdir = xdir - normal.unitVector() * xdir.dot(normal.unitVector());
   projxdir.normalize();
  
   rot[0] = projxdir;
   rot[2] = normal.unitVector();
   rot[1] = rot[2].cross(rot[0]);

   altmaterial = NULL;
}

Tiled_Plane_Object::~Tiled_Plane_Object(){

   if(altmaterial) delete altmaterial;

}

void Tiled_Plane_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="normal"){
         cin>>normal;
      }else if(ID=="point"){
         cin>>point;
      }else if(ID=="xdir"){
         cin>>xdir;
      }else if(ID=="dimension"){
         cin>>dimX>>dimY;
      }else if(ID=="altmaterial"){
         cin>>altMatName;
      }else{
         cerr<<"Incorrect Tiled_Plane_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }

   projxdir = xdir - normal.unitVector() * xdir.dot(normal.unitVector());
   projxdir.normalize();
  
   rot[0] = projxdir;
   rot[2] = normal.unitVector();
   rot[1] = rot[2].cross(rot[0]);

}

void Tiled_Plane_Object::dumper() const{
   Plane_Object::dumper();
   cerr<<"---XDir: "<<xdir<<endl;
   cerr<<"---Dimensions: "<<dimX<<"x"<<dimY<<endl;
   cerr<<"---AltMaterial: "<<altMatName<<endl;
}

bool Tiled_Plane_Object::select() const{

   Vec3d newpoint = Vec3d(hitPt->dot(rot[0]), hitPt->dot(rot[1]), hitPt->dot(rot[2]));
   newpoint += 100000;
   int tileNum = (int)(newpoint.x/dimX) + (int)(newpoint.y/dimY);
   return tileNum%2==0;

}

void Tiled_Plane_Object::connectObjects(const Model* model){
   for(list<Material*>::iterator Mi = model->materials->begin(); Mi != model->materials->end(); ++Mi){
      if (matName == (*Mi)->name)
         material = *Mi;
      if (altMatName == (*Mi)->name)
         altmaterial = *Mi;
   }
}

Vec3d Tiled_Plane_Object::getamb() const{
   if (select())
      return altmaterial->ambient;
   else
      return material->ambient;
}

Vec3d Tiled_Plane_Object::getdiff() const{
   if (select())
      return altmaterial->diffuse;
   else
      return material->diffuse;
}

Vec3d Tiled_Plane_Object::getspec() const{
   if (select())
      return altmaterial->specular;
   else
      return material->specular;
}
