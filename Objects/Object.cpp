#include "Object.h"
#include "../Model.h"

Object::Object(){
   name = "NAME";
   material = NULL;
   hitPt = new Vec3d();
}

Object::Object(string n):name(n){
   material = NULL;
   hitPt = new Vec3d();
}

Object::~Object(){

   if(hitPt) delete hitPt;
   if(material) delete material;

}

double Object::hittest(const Ray ray){
   cerr<<"Object hittest called"<<endl;
   return -1;
}  

void Object::parse(){
   cerr<<"Object parse called"<<endl;
}  

Vec3d Object::getNormal() const{
   cerr<<"Object getNormal called"<<endl;
   return Vec3d(0,0,0);
}

void Object::dumper() const{
   cerr<<name<<endl;
   cerr<<"---Material: "<<material->name<<endl;
}

void Object::connectObjects(const Model *model){
   for(list<Material*>::iterator Mi = model->materials->begin(); Mi != model->materials->end(); ++Mi){
      if (matName == (*Mi)->name)
      material = *Mi;
   }
}

Vec3d Object::getamb() const{
   return material->ambient;
}

Vec3d Object::getdiff() const{
   return material->diffuse;
}

Vec3d Object::getspec() const{
   return material->specular;
}

Vec3d Object::getHitPt() const{
   return *hitPt;
}
