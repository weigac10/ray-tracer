#include "Triangle_Object.h"
#include "../Model.h"

Triangle_Object::Triangle_Object() : Object(){
   normal = Vec3d(0,0,0);
   points[0] = points[1] = points[2] = Vec3d(0,0,0);
}

Triangle_Object::~Triangle_Object(){



}

void Triangle_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="normal"){
         cin>>normal;
      }else if(ID=="pointA"){
         cin>>points[0];
      }else if(ID=="pointB"){
         cin>>points[1];
      }else if(ID=="pointC"){
         cin>>points[2];
      }else{
         cerr<<"Incorrect Triangle_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }
}

//Moller-Trumbore Triangle Intersection Algorithm
double Triangle_Object::hittest(const Ray ray){
   Vec3d edge1 = points[1] - points[0];
   Vec3d edge2 = points[2] - points[0];
   Vec3d pvec = ray.direction.cross(edge2);
   double det = edge1.dot(pvec);
   if (det == 0)
      return -1;
   double invDet = 1 / det;
   Vec3d tvec = ray.position - points[0];
   double u = tvec.dot(pvec) * invDet;
   if (u < 0 || u > 1)
      return -1;
   Vec3d qvec = tvec.cross(edge1);
   double v = ray.direction.dot(qvec) * invDet;
   if (v < 0 || u + v > 1)
      return -1;
   double t = edge2.dot(qvec) * invDet;
 
   *hitPt = ray.position + ray.direction * t;
   return t;
}

Vec3d Triangle_Object::getNormal() const{
   return normal;
}

void Triangle_Object::dumper() const{
   Object::dumper();
   cerr<<"---Normal: "<<normal<<endl;
   cerr<<"---Points: "<<points[0]<<" "<<points[1]<<" "<<points[2]<<endl;
}
