#ifndef OBJECT
#define OBJECT

#include "../Material.h"
#include "../Ray.h"
#include "../Vec3d.h"
#include <iostream>
#include <list>
using namespace std;

class Model;

class Object{

   public:
      string name, matName;
      Material *material;

      Object();
      ~Object();
      Object(string n);
      virtual double hittest(const Ray ray);
      virtual void parse();
      virtual Vec3d getNormal() const;
      virtual void dumper() const;
      virtual void connectObjects(const Model *model);
      virtual Vec3d getamb() const;
      virtual Vec3d getdiff() const;
      virtual Vec3d getspec() const;
      Vec3d getHitPt() const;

   protected:
      Vec3d *hitPt;

};

#endif
