#ifndef TILED_PLANE_OBJECT
#define TILED_PLANE_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "Plane_Object.h"
#include "../Ray.h"
#include "../Vec3d.h"
using namespace std;

class Tiled_Plane_Object : public Plane_Object{

      public:
         Tiled_Plane_Object(string n);
         ~Tiled_Plane_Object();
         void parse();
         virtual void dumper() const;
         bool select() const;
         virtual void connectObjects(const Model *model);
         virtual Vec3d getamb() const;
         virtual Vec3d getdiff() const;
         virtual Vec3d getspec() const;

      private:
         Vec3d rot[3];
         Vec3d xdir, projxdir;
         double dimX, dimY;
         string altMatName;
         Material *altmaterial;

};

#endif
