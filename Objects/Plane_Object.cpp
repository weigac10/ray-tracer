#include "Plane_Object.h"
#include "../Model.h"

Plane_Object::Plane_Object(string n) : Object(n){
   normal = Vec3d(0,0,1);
   point = Vec3d(0,0,0);
}

Plane_Object::~Plane_Object(){



}

void Plane_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="normal"){
         cin>>normal;
      }else if(ID=="point"){
         cin>>point;
      }else{
         cerr<<"Incorrect Plane_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }
}

double Plane_Object::hittest(const Ray ray){
   double tNumer = normal.dot(point) - normal.dot(ray.position);
   double tDenom = normal.dot(ray.direction);
   double t;
   if (tNumer && tDenom){
      t = tNumer/tDenom;
      *hitPt = ray.position + ray.direction * t;
      return t;
   }else
      return -1;
}

Vec3d Plane_Object::getNormal() const{
   return normal.unitVector();
}

void Plane_Object::dumper() const{
   Object::dumper();
   cerr<<"---Normal: "<<normal<<endl;
   cerr<<"---Point: "<<point<<endl;
}
