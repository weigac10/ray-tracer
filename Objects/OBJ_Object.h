#ifndef OBJ_OBJECT
#define OBJ_OBJECT

#include <list>
#include <fstream>
#include <iostream>
#include <string.h>

#include "Object.h"
#include "../mtl.h"
#include "../Ray.h"
#include "../Vec3d.h"
#include "../rayFuncs.h"
using namespace std;

struct facePoint{

   Vec3d position, texture, normal;
   int material;

};

class OBJ_Object : public Object{

      public:
         OBJ_Object() : Object(){

         }

         OBJ_Object(string objFileName, string mtlFileName) : Object(){
            loadFile(objFileName, mtlFileName);
         }

         virtual void parse(){
            
            string objFile="", mtlFile="";
            string ID;
            cin>>ID;
            while(ID!="}"){
               if(ID=="obj"){
                  cin>>objFile;
               }else if(ID=="mtl"){
                  cin>>mtlFile;
               }else{
                  cerr<<"Incorrect OBJ_Object in input file. Exiting..."<<endl;
                  exit(0);
               }
               cin>>ID;
            }
            loadFile(objFile, mtlFile);
         }

         virtual double hittest(const Ray ray){
            list< list <facePoint> >::iterator li = modelFaces.begin();
            double dist = INT_MAX;
            while(li != modelFaces.end()){
               list<facePoint>::iterator it = (*li).begin();
               int numPoints = (*li).size();
               Vec3d *points = new Vec3d[numPoints];
               for(int i=0;i<numPoints;i++){
                  points[i] = (*it).position;
                  ++it;
               }
               switch(numPoints){
                  case 3:{
                     //double t = triangleIntersection(ray, points);
                     //if (t!=-1 && t<dist){
                     //   dist=t;
                        //cerr<<ray.direction+ray.position<<endl;
                        //cerr<<points[0]<<" "<<points[1]<<" "<<points[2]<<endl;
                    // }
                     break;
                  }
                  case 4:{
                     Vec3d triPoints1[3] = {points[0], points[1], points[2]};
                     Vec3d triPoints2[3] = {points[0], points[2], points[3]};
                     cerr<<triPoints1[0]<<" "<<triPoints1[1]<<" "<<triPoints1[2]<<endl;
                     cerr<<triPoints2[0]<<" "<<triPoints2[1]<<" "<<triPoints2[2]<<endl;
                     //double t1 = triangleIntersection(ray, triPoints1);
                     //double t2 = triangleIntersection(ray, triPoints2);
                     //if(t1!=-1 && t1<dist) dist=t1;
                     //if(t2!=-1 && t2<dist) dist=t2;
                     break;
                  }
                  default:
                     cerr<<"Unknown polygon type: "<<numPoints<<"-gon"<<endl;
               }
               ++li;
            }
            return (dist==INT_MAX)?-1:dist;
         }

      private:
         list< list<facePoint> > modelFaces;

         void loadFile(string objFileName, string mtlFileName){
         /*
          *         Initialize a linked list of triangles from an obj file
          *         Input is the filename of the obj and mtl files without an extension
          *         Output is a linked list
          */

            list<Vec3d> modelVerts;
            list<Vec3d> modelNorms;
            list<Vec3d> modelTexs;
            list<mtl> modelMats;

            Vec3d avgCoords, maxCoords;
            int numVerts, numNorms, numTexs;
            double maxCoord;

         //---------------------------MTL--------------------------------------  
            int i=0;
            fstream mtlFile;
            mtlFile.open(mtlFileName.c_str(), ios::in);
 
            bool skipMtl = false;
            if(!mtlFile.is_open()){
               cerr<<"Could not find file "<<mtlFileName<<endl;
               cerr<<"Skipping materials"<<endl;
               skipMtl = true;
            }
            if(!skipMtl){
               string input,mtlName;
               mtl curMtl;
               curMtl.clear();
               float a,b,c;
               while(mtlFile>>input){
                  if(input == "newmtl"){ //New mtl name coming
                     modelMats.push_back(curMtl); //Add current material to list(will be default the first time
                     curMtl.clear();        //Reset the current material
                     mtlFile>>curMtl.name;
                  }else if(input == "Ka"){ //ambient coming
                     mtlFile>>a>>b>>c;
                     curMtl.ambient.set(a,b,c);
                  }else if(input == "Kd"){ //diffuse coming
                     mtlFile>>a>>b>>c;
                     curMtl.diffuse.set(a,b,c);
                  }else if(input == "Ks"){ //specular coming
                     mtlFile>>a>>b>>c;
                     curMtl.specular.set(a,b,c);
                  }else if(input == "Tf"){ //transmission filter coming
                     mtlFile>>a>>b>>c;
                     curMtl.transFilter.set(a,b,c);
                  }else if(input == "Ni"){ //Optical Density coming
                     mtlFile>>curMtl.optDens;
                  }else if(input == "illum"){ //Illumination coming
                     mtlFile>>curMtl.illum;
                  }else if(input == "Ns"){ //Shininess coming
                     mtlFile>>curMtl.shine;
                  }
               }
               modelMats.push_back(curMtl); //Add the final material
               mtlFile.close();
            }

            //--------------------------OBJ---------------------------------------  

            i=0;
            fstream objFile;
            objFile.open(objFileName.c_str(), ios::in);

            if(!objFile.is_open()){
               cerr<<"Could not find file "<<objFileName<<endl;
               cerr<<"Quitting"<<endl;
               exit(0);
            }
            char var;
            char junk[1000];
            numVerts=numNorms=numTexs=0;
            int currentMtl=1;
            while(objFile>>var){ //This should grab the first character on a line for 'good' lines'
               switch(var){
                  case 'v':{ //Vertex something
                     switch(objFile.peek()){
                        case ' ':{ //Vertex point
                           double x,y,z;
                           objFile>>x>>y>>z; //Cin 3 coords
                           Vec3d m(x,y,z);
                           modelVerts.push_back(m); //Add vertex to modelVerts
                           avgCoords = avgCoords + Vec3d(x,y,z);
                           numVerts++;
                           if(x<0)x*=-1;
                           if(y<0)y*=-1;
                           if(z<0)z*=-1;
                           if(x>maxCoords.x)maxCoords.x=x;
                           if(y>maxCoords.y)maxCoords.y=y;
                           if(z>maxCoords.z)maxCoords.z=z;
                        }
                        break;
                        case 't':{ //Vertex texture
                           objFile.get();
                           double x,y,z;
                           x=y=z=0;
                           objFile>>x;
                           char in = objFile.peek();
                           while(in==' ' && in != '\n'){
                              objFile.get();
                              in = objFile.peek(); 
                           }
                           if(in!='\n'){
                              objFile>>y;
                              in = objFile.peek();
                              while(in==' ' && in != '\n'){
                                 objFile.get();
                                 in = objFile.peek(); 
                              }
                           }
                           if(in!='\n'){
                              objFile>>z;
                              in = objFile.peek();
                           }
                           Vec3d m(x,y,z);
                           modelTexs.push_back(m);
                           numTexs++;
                        }
                        break;
                        case 'n':{ //Vertex normal
                           objFile.get();
                           double x,y,z;
                           objFile>>x>>y>>z;
                           Vec3d m(x,y,z);
                           modelNorms.push_back(m);
                           numNorms++;
                        }
                        break;
                     }
                  }
                  break;
                  case 'f':{ //Face
                     char in;
                     list<facePoint> face;
                     in = objFile.get(); //should be a space
                     while(in!='\n'){ //Go through the line grabbing all of the vertice positions
                        int vertexPos;
                        objFile>>vertexPos; //Scan in position
                        list<Vec3d>::iterator li = modelVerts.begin(); //Iterate through vertices to find position. Same as modelVerts[pos-1];
                        for(int i=0;i<numVerts-vertexPos;i++){
                           ++li;
                        }
                        Vec3d vertex = *li;
                        Vec3d texture;
                        Vec3d normal(0,0,1);
                        char next = objFile.peek(); //peek for /
                        if(next =='/'){
                           next = objFile.get(); //grab the /
                           next = objFile.peek(); //peek for / or number
                           if(isdigit(next)){ //A texture exists
                              int texturePos;
                              objFile>>texturePos;
                              li = modelTexs.begin(); //Iterate through norms to find position. Same as modelVerts[numVerts-pos-1];
                              for(int i=0;i<numTexs-texturePos;i++){
                                 ++li;
                              }
                              texture = *li;
                           }
                        }
                        next = objFile.peek();
                        if(next =='/'){
                           next = objFile.get(); //get the /
                           int normalPos;
                           if(objFile.peek()!=' '){ //A normal exists
                              objFile>>normalPos; //Vertex normal
                              li = modelNorms.begin(); //Iterate through norms to find position. Same as modelVerts[numVerts-pos-1];
                              for(int i=0;i<numNorms-normalPos;i++){
                                 ++li;
                              }
                              normal = *li;
                           }
                        }
                        facePoint finalPoint;
                        finalPoint.position = vertex;
                        finalPoint.texture = texture;
                        finalPoint.normal = normal;
                        finalPoint.material = currentMtl;
                        face.push_back(finalPoint);
                        in = objFile.peek();
                        while(in==' ' && in!='\n'){ //Grabs trailing whitespace
                           objFile.get(); //should be a space or newline
                           in = objFile.peek();
                        }
                     }
                     objFile.get(); //grab the newline
                     modelFaces.push_back(face);
                  }
                  break;
                  case 'u':{
                     if(!skipMtl){
                        objFile.putback(var);
                        string usemtl,mtlName;
                        objFile>>usemtl>>mtlName;
                        list<mtl>::iterator li= modelMats.begin();
                        int m=0;
                        while((*li).name != mtlName){
                           ++li;
                           m++;
                        }
                        currentMtl = m;
                     }
                  break;
                  }
                  default:
                     objFile.getline(junk,1000); //Ignore comments and other nuisances
               }
            }
            objFile.close();

            avgCoords = avgCoords/numVerts;
            maxCoord = (maxCoords.x+avgCoords.x>maxCoords.y+avgCoords.y)?maxCoords.x+avgCoords.x:maxCoords.y+avgCoords.y;
            maxCoord = (maxCoord>maxCoords.z+avgCoords.z)?maxCoord:maxCoords.z+avgCoords.z;
         }

};

#endif
