#include "Sphere_Object.h"
#include "../Model.h"

Sphere_Object::Sphere_Object(string n) : Object(n){
   center = Vec3d(0,0,0);
   radius = 0;
}

Sphere_Object::~Sphere_Object(){



}

void Sphere_Object::parse(){

   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="material"){
         cin>>matName;
      }else if(ID=="center"){
         cin>>center;
      }else if(ID=="radius"){
         cin>>radius;
      }else{
         cerr<<"Incorrect Sphere_Object in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }
}

double Sphere_Object::hittest(const Ray ray){
   double a = ray.direction.dot(ray.direction);
   double b = ray.direction.dot(ray.position-center) * 2;
   double c = (ray.position-center).dot(ray.position-center) - radius*radius;

   double discriminant = b*b - 4*a*c;
   if(discriminant <= 0){
      return -1;
   }else{
      double t = (-b-sqrt(discriminant))/(2*a);
      *hitPt = ray.position + ray.direction * t;
      return t;
   }
}

Vec3d Sphere_Object::getNormal() const{
   return (*hitPt - center).unitVector();
}

void Sphere_Object::dumper() const{
   Object::dumper();
   cerr<<"---Center: "<<center<<endl;
   cerr<<"---Radius: "<<radius<<endl;
}
