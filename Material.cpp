#include "Material.h"
 
Material::Material(string n):name(n){
   diffuse = ambient = specular = Vec3d(0,0,0);
}

Material::~Material(){



}

void Material::parse(){
   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="diffuse"){
         cin>>diffuse;
      }else if(ID=="ambient"){
         cin>>ambient;
      }else if(ID=="specular"){
         cin>>specular;
      }else{
         cerr<<"Incorrect Material in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }
}

void Material::dumper(){
   cerr<<name<<endl;
   cerr<<"---Diffuse: "<<diffuse<<endl;
   cerr<<"---Ambient: "<<ambient<<endl;
   cerr<<"---Specular: "<<specular<<endl;
}
