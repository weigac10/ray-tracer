#ifndef MODEL
#define MODEL

#include <list>

#include "Camera.h"
#include "Light.h"
#include "Material.h"

#include "Objects/Object.h"
#include "Objects/Plane_Object.h"
#include "Objects/Finite_Plane_Object.h"
#include "Objects/Tiled_Plane_Object.h"
#include "Objects/Textured_Plane_Object.h"
#include "Objects/Sphere_Object.h"
#include "Objects/Triangle_Object.h"
using namespace std;

class Model{

   public:
      list<Object*> *objects;
      list<Material*> *materials;
      list<Light*> *lights;
      Camera *cam;

      Model();
      ~Model();
      void parseInput();
      void connectObjects();
      void dumper();

};

#endif
