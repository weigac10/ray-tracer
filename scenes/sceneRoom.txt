camera cam1
{
   pixeldim 1280 800
   worlddim 8 5
   viewpoint 4 3 4
}

material white
{
   diffuse 1 1 1
   ambient 1 1 1
   specular 1 1 1
}

material black
{
   diffuse 0 0 0
   ambient 0 0 0
}

material mirror
{
   ambient 1 1 1
   specular 1 1 1
}

sphere ball
{
   material mirror
   center 4 3 -4
   radius 2
}

tiled_plane frontwall
{
   material black
   normal 0 0 1
   xdir 1 0 0
   point 0 0 -6
   dimension 1.0 1.0
   altmaterial white
}

tiled_plane floor
{
   material black
   normal 0 1 0
   xdir 1 0 0
   point 0 0 0
   dimension 1.0 1.0
   altmaterial white
}

tiled_plane backwall
{
   material black
   normal 0 0 -1
   xdir -1 0 0
   point 0 0 6
   dimension 1.0 1.0
   altmaterial white
}

tiled_plane ceiling
{
   material black
   normal 0 -1 0
   xdir 1 0 0
   point 0 6 0
   dimension 1.0 1.0
   altmaterial white
}

tiled_plane leftwall
{
   material black
   normal 1 0 0
   xdir 0 0 1
   point 0 0 0
   dimension 1.0 1.0
   altmaterial white
}

tiled_plane rightwall
{
   material black
   normal -1 0 0
   xdir 0 0 -1
   point 6 0 0
   dimension 1.0 1.0
   altmaterial white
}

light ALight
{
   location 1 1 1
   emissivity 0 3 6
}
