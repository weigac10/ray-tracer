camera cam1
{
   pixeldim 640 480
   worlddim 8 6
   viewpoint 4 3 4
}

material green
{
   diffuse 0 1 0
   ambient 0 5 0
}

material purple
{
   diffuse 0 1 0
   ambient 3 0 3
}

material yellow
{
   diffuse 4 4 0
   ambient 5 4 0
   specular 1 1 1
}
material blue
{
   diffuse 4 4 0
   ambient 0 0 3
   specular 1 1 1
}

material gray
{
   ambient 2 2 2
}

sphere bb 
{
   material purple
   center 3 4 -2
   radius 2 
}

tiled_plane floor
{
   material yellow
   normal 0 1 0
   point 0 0 0
   dimension 1.0 1.0
   altmaterial green
}


light red-ceiling
{
   location 4 4 -2
   emissivity 5 1 1
}
light blue-floor
{
   location 2 0 0
   emissivity 1 1 5
}
