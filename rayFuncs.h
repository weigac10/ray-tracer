#ifndef RAYFUNCS
#define RAYFUNCS

#define MAX_DIST 50

#include <iostream>
#include <list>

#include "Ray.h"
#include "Vec3d.h"
#include "Model.h"

using namespace std;

void make_pixels(const Model *model);
void raytrace(const Model *model, const Ray &ray, Vec3d &color, double totalDist, const Object* lastObject);
const Object* find_closest_object(const list<Object*> *objects, const Object *lastObject, const Ray &ray, double &dist);
Vec3d illuminate(const Model *model, const Object* ob);
void dumpHeader(int width, int height);

#endif
