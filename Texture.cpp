#include "Texture.h"

Texture::Texture(string n):name(n){

   filebuf fb;
   if (fb.open(name.c_str(), ios::in)){
      istream is(&fb);

      char junk[1000];
      is.getline(junk, 1000);
      is.getline(junk, 1000);
      is>>xdim>>ydim;
      is.getline(junk, 1000);
      is.get();

      imagebuf = new unsigned char[xdim*ydim*3];
      for(int i=0;i<xdim*ydim;i++){
         imagebuf[i*3] = is.get();
         imagebuf[i*3+1] = is.get();
         imagebuf[i*3+2] = is.get();
      }
     
      fb.close();
   }else{
      cerr<<"Error - Could not find texture file "<<name<<".  Quitting..."<<endl;
      exit(1);
   }
   
}

Texture::~Texture(){

   delete imagebuf;

}

void Texture::texture_fit(const double relX, const double relY, Vec3d &texel) const{

   gettexel(relX * xdim, relY * ydim, texel);

}

void Texture::texture_tile(const Vec3d newpoint, Vec3d &texel) const{

   int absX = ((int)(newpoint.x/pixelSizeX) % xdim);
   int absY = ((int)(newpoint.y/pixelSizeY) % ydim);

   gettexel(absX, absY, texel);
}

void Texture::gettexel(const int pixX, const int pixY, Vec3d &texel) const{

   int pixelnum = (ydim - pixY - 1) * xdim + pixX;
   texel = Vec3d(imagebuf[3*pixelnum], imagebuf[3*pixelnum + 1], imagebuf[3*pixelnum + 2]) / 255;

}

void Texture::getdim(int *x,int *y) const{
   *x = xdim;
   *y = ydim;
}

void Texture::setPixelSize(const double psX, const double psY){
   pixelSizeX = psX;
   pixelSizeY = psY;
}

void Texture::dumper() const{
   cerr<<name<<endl;
   cerr<<"---Dimensions: "<<xdim<<"x"<<ydim<<endl;
}
