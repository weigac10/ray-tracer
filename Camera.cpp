#include "Camera.h"

Camera::Camera(string n): name(n){ }

Camera::Camera(int w, int h, double worldX, double worldY, Vec3d pos){
   pixel_dim_w = w;
   pixel_dim_h = h;
   world_dim_w = worldX; 
   world_dim_h = worldY;
   view_point = pos;
}

Camera::~Camera(){



}

void Camera::parse(){
   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="pixeldim"){
         cin>>pixel_dim_w>>pixel_dim_h;
      }else if(ID=="worlddim"){
         cin>>world_dim_w>>world_dim_h;
      }else if(ID=="viewpoint"){
         cin>>view_point;
      }else{
         cerr<<"Incorrect Camera in input file. Exiting..."<<endl;
         exit(0);
      }
      cin>>ID;
   }
}

int Camera::getpx(){
   return pixel_dim_w;
}

int Camera::getpy(){
   return pixel_dim_h;
}

double Camera::getWorldX(){
   return world_dim_w;
}

double Camera::getWorldY(){
   return world_dim_h;
}

Vec3d Camera::getviewpt(){
   return view_point;
}

void Camera::dumper(){
   cerr<<name<<endl;
   cerr<<"---Pixel_dimensions: "<<pixel_dim_w<<"x"<<pixel_dim_h<<endl;
   cerr<<"---World_dimensions: "<<world_dim_w<<"x"<<world_dim_h<<endl;
   cerr<<"---Location: "<<view_point<<endl;
}
