#include "Light.h"

Light::Light(string n):name(n){
   location = Vec3d(0,0,0);
   emissivity = Vec3d(0,0,0);
}

Light::~Light(){



}

void Light::parse(){
   string ID;
   cin>>ID;
   while(ID!="}"){
      if(ID=="location"){
         cin>>location;
      }else if(ID=="emissivity"){
         cin>>emissivity;
      }else{
         cerr<<"Incorrect Light in input file. Skipping..."<<endl;
      }
      cin>>ID;
   }
}

void Light::dumper(){
   cerr<<name<<endl;
   cerr<<"---Location: "<<location<<endl;
   cerr<<"---Emissivity: "<<emissivity<<endl;
}
