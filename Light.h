#ifndef LIGHT
#define LIGHT

#include "Vec3d.h"

#include <iostream>
using namespace std;

class Light{

   public:
      string name;
      Vec3d location, emissivity;
      
      Light(string n);
      ~Light();
      virtual void parse();
      void dumper();

};

#endif
