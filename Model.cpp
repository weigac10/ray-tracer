#include "Model.h"

Model::Model(){
   objects = new list<Object*>();
   materials = new list<Material*>();
   lights = new list<Light*>();
   parseInput();
   connectObjects();
}

Model::~Model(){

   for(list<Object*>::iterator li = objects->begin(); li!=objects->end(); ++li)
      delete *li;
   for(list<Material*>::iterator li = materials->begin(); li!=materials->end(); ++li)
      delete *li;
   for(list<Light*>::iterator li = lights->begin(); li!=lights->end(); ++li)
      delete *li;

   delete objects;
   delete materials;
   delete lights;
   delete cam;
}
   
void Model::parseInput(){

   string junk;
   string type, name;
   while(cin>>type>>name){
      cin>>junk; //{
      Object *object = NULL;
      if(type=="camera"){
         cam = new Camera(name);
         cam->parse();
         continue;
      }else if(type=="material"){
         Material* m = new Material(name);
         m->parse();
         materials->push_back(m);
         continue;
      }else if(type=="light"){
         Light* Li = new Light(name);
         Li->parse();
         lights->push_back(Li);
         continue;
      }else if(type=="triangle"){
         object = new Triangle_Object();
      }else if(type=="plane"){
         object = new Plane_Object(name);
      }else if(type=="finite_plane"){
         object = new Finite_Plane_Object(name);
      }else if(type=="tiled_plane"){
         object = new Tiled_Plane_Object(name);
      }else if(type=="textured_plane"){
         object = new Textured_Plane_Object(name);
      }else if(type=="sphere"){
         object = new Sphere_Object(name);
      }else if(type=="obj"){
         //object = new OBJ_Object();
      }else{
         cerr<<"Unknown object \""<<type<<" "<<name<<"\" in input file.  Skipping..."<<endl;
         continue;
      }
      object->parse(); //Must loop until cin>>} so that file pointer is after }
      objects->push_back(object);
   }
}

void Model::connectObjects(){
   for(list<Object*>::iterator li = objects->begin(); li!=objects->end(); ++li){
      (*li)->connectObjects(this);
   }
}

void Model::dumper(){

   cerr<<"------------CAMERA-------------"<<endl;
   cam->dumper();
   cerr<<"------------OBJECTS-------------"<<endl;
   for(list<Object*>::const_iterator li = objects->begin(); li!=objects->end(); ++li)
      (*li)->dumper();
   cerr<<"------------MATERIALS-------------"<<endl;
   for(list<Material*>::const_iterator li = materials->begin(); li!=materials->end(); ++li)
      (*li)->dumper();
   cerr<<"------------LIGHTS-------------"<<endl;
   for(list<Light*>::const_iterator li = lights->begin(); li!=lights->end(); ++li)
      (*li)->dumper();
}
