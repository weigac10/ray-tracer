#ifndef TEXTURE
#define TEXTURE

#include <fstream>
#include <iostream>
#include <string.h>

#include "Ray.h"
#include "Vec3d.h"
using namespace std;

class Texture{

      public:
         string name;
         double pixelSizeX, pixelSizeY;

         Texture(string n);
         ~Texture();
         void gettexel(const int pixX, const int pixY, Vec3d &texel) const;
         void texture_fit(const double relX, const double relY, Vec3d &texel) const;
         void texture_tile(const Vec3d newpoint, Vec3d &texel) const;
         void getdim(int *x,int *y) const;
         void setPixelSize(const double psX, const double psY);
         void dumper() const;

         private:
            int xdim, ydim;
            unsigned char *imagebuf;
};

#endif
