//Probably missing some less important components of a .mtl file
#ifndef MTL
#define MTL

#include<string>

#include "Vec3d.h"
using namespace std;


class mtl{
   public:
      string name;
      Vec3d ambient,diffuse,specular,transFilter;
      float illum,shine,optDens;

      void clear(){
         name = "";
         ambient.set(0.2,0.2,0.2);
         diffuse.set(0.8,0.8,0.8);
         specular.set(1.0,1.0,1.0);
         transFilter.set(1.0,1.0,1.0);

         shine = 0;
         illum = 1;
         optDens = 1.0;
      }

};

#endif
