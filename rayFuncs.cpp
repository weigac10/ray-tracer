#include "rayFuncs.h"

void make_pixels(const Model *model){

   dumpHeader(model->cam->getpx(), model->cam->getpy());
   for (int i=model->cam->getpy();i>=0;i--){
      for (int j=0;j<model->cam->getpx();j++){
         Vec3d color;

         //Get ray direction
         double worldX = model->cam->getWorldX() * (((double)j)/(model->cam->getpx()-1));
         double worldY = model->cam->getWorldY() * (((double)i)/(model->cam->getpy()-1));
         const Ray ray(model->cam->getviewpt(), Vec3d(worldX, worldY, 0) - model->cam->getviewpt());

         raytrace(model, ray, color, 0, NULL);

         color *= 255;
         color.clamp(0,255);
         cout<<(unsigned char)color.x<<(unsigned char)color.y<<(unsigned char)color.z;
      }
   }
}

void raytrace(const Model *model, const Ray &ray, Vec3d &color, double totalDist, const Object* lastObject){

   Vec3d thisRay;

   //Get closest object, distance, and hitpoint
   double dist;
   const Object *ob = find_closest_object(model->objects, lastObject, ray, dist);
   totalDist += dist;
   if (ob == NULL || totalDist > MAX_DIST){
      return;
   }

   //Calculate ambient contribution
   thisRay += ob->getamb();

   //Calculate diffuse contribution
   thisRay += illuminate(model, ob);

   thisRay /= totalDist;

   //Calculate specular contribution
   Vec3d specularity = ob->getspec();
   if (!specularity.isZero()){

      //Calculate reflected ray
      Vec3d normal = ob->getNormal();
      Vec3d newDir = normal * 2 * (-ray.direction).dot(normal) + ray.direction; 
      const Ray newRay(ob->getHitPt(), newDir);
      
      //Ray trace again
      Vec3d specInt;
      raytrace(model, newRay, specInt, totalDist, ob);
      thisRay += specInt*specularity;
   }

   color += thisRay;
}

const Object* find_closest_object(const list<Object*> *objects, const Object *lastObject, const Ray &ray, double &dist){
   dist = INT_MAX;
   Object* ob = NULL;
   for(list<Object*>::const_iterator li = objects->begin(); li != objects->end(); ++li){
      if( *li == lastObject ) continue;
      double d = (*li)->hittest(ray);
      if (d>0 && d<dist){
         dist = d; 
         ob = *li;
      }
   }
   return ob;
}

Vec3d illuminate(const Model *model, const Object* ob){

   Vec3d color;

   for(list<Light*>::const_iterator li = model->lights->begin(); li!=model->lights->end(); ++li){
      Vec3d vecToLight = (*li)->location - ob->getHitPt();
      Ray rayToLight(ob->getHitPt(), vecToLight);

      double cos = (rayToLight.direction).dot(ob->getNormal());
      if(cos > 0){ //If not self-occluding (if the angle < 90)
         double dist;
         find_closest_object(model->objects, ob, rayToLight, dist);
         if(dist > vecToLight.length()){ //If no object hit (dist==INT_MAX) or light is closer than object
            Vec3d colorFromLight = ob->getdiff() * (*li)->emissivity;
            double scaleFactor = cos / vecToLight.length();
            colorFromLight *= scaleFactor;
            color += colorFromLight;
         }
      }
   }
   return color;
}

void dumpHeader(int width, int height){
   cout<<"P6"<<endl;
   cout<<"#Output image from ray tracer"<<endl;
   cout<<width<<" "<<height<<endl;
   cout<<"255"<<endl;
}
