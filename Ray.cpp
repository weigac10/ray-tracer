#include "Ray.h"

Ray::Ray(){
   position.set(0,0,0);
   direction.set(0,0,1);
}

Ray::Ray(Vec3d p, Vec3d d):position(p),direction(d.unitVector()){ }

Ray::~Ray(){



}

void Ray::normalize(){
   direction.normalize();
}
