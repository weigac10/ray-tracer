#ifndef MATERIAL
#define MATERIAL

#include <iostream>
#include <string.h>

#include "Ray.h"
#include "Vec3d.h"
using namespace std;

class Material{

      public:
         string name;
         Vec3d diffuse, ambient, specular;
 
         Material(string n);
         ~Material();
         void parse();
         void dumper();

};

#endif
