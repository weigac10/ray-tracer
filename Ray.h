#ifndef RAY
#define RAY

#include "Vec3d.h"

class Ray{

   public:
      Vec3d position;
      Vec3d direction;

      Ray();
      ~Ray();
      Ray(Vec3d p, Vec3d d);
      void normalize();

};

#endif
