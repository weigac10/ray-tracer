CC=g++
CFLAGS=-c -Wall
DFLAGS=-DDEBUG -g
OUTFILE=rayTrace
SRC=rayTrace.cpp rayFuncs.cpp Vec3d.cpp Ray.cpp Texture.cpp Camera.cpp Material.cpp Light.cpp Model.cpp $(wildcard Objects/*.cpp)
OBJ=$(SRC:.cpp=.o)
SCENEDIR=scenes/
IMAGEDIR=images/
SCENE= #Assign to SCENE which scene file to read from for full make and run
SCENEFILE=scene$(SCENE).txt
IMAGEFILE=image$(SCENE).ppm
ERRORFILE=errors.txt

all: main

main: $(OBJ)
	$(CC) $(OBJ) -o $(OUTFILE)

%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@

debug: CFLAGS+=$(DFLAGS)
debug: main

run:
	./$(OUTFILE) < $(SCENEDIR)$(SCENEFILE) > $(IMAGEDIR)$(IMAGEFILE) 2> $(ERRORFILE)

full: main run

clean:
	-/bin/rm *.o Objects/*.o $(OUTFILE)

cleanall: clean
	-/bin/rm $(IMAGEDIR)/*.ppm

rebuild: clean main
