/*
   RayTrace.cpp
   Christian Weigandt
   Started - August 30, 2013  

*/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Model.h"
#include "rayFuncs.h"

int main(int argc, char** argv){

   Model *model = new Model();
   model->dumper();
   make_pixels(model);

   return 0;

}

